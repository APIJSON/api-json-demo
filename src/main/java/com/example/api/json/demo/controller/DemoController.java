/*Copyright ©2016 TommyLemon(https://github.com/TommyLemon/APIJSON)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package com.example.api.json.demo.controller;

import apijson.JSONResponse;
import apijson.framework.APIJSONController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Objects;


/**
 * 数据相关的控制器，包括通用增删改查接口等
 * <br > 建议全通过HTTP POST来请求:
 * <br > 1.减少代码 - 客户端无需写HTTP GET,PUT等各种方式的请求代码
 * <br > 2.提高性能 - 无需URL encode和decode
 * <br > 3.调试方便 - 建议使用 APIAuto(http://apijson.cn/api) 或 Postman
 *
 * @author Lemon
 */
@RestController
public class DemoController extends APIJSONController {
    private static final String TAG = "DemoController";

    /**
     * 获取
     *
     * @param request 只用String，避免encode后未decode
     * @param session /
     * @return /
     */
    @PostMapping(value = "get", consumes = MediaType.ALL_VALUE)
    @Override
    public String get(@RequestBody String request, HttpSession session) {
        return covertOwnFormat(super.get(request, session));
    }

    /**
     * 转化为嘉联的返回格式
     *
     * @param frameworkResp 原来的返回格式
     * @return /
     */
    private String covertOwnFormat(String frameworkResp) {
        JSONObject jsonObject = JSON.parseObject(frameworkResp);
        jsonObject.remove(JSONResponse.KEY_OK);
        jsonObject.put("retCode", Objects.equals(JSONResponse.CODE_SUCCESS, jsonObject.get(JSONResponse.KEY_CODE)) ? "00" : "01");
        jsonObject.put("retMsg", jsonObject.get(JSONResponse.KEY_MSG));
        jsonObject.remove(JSONResponse.KEY_CODE);
        jsonObject.remove(JSONResponse.KEY_MSG);
        return jsonObject.toJSONString();
    }

    /**
     * 限制性GET，request和response都非明文，浏览器看不到，用于对安全性要求高的GET请求
     *
     * @param request 只用String，避免encode后未decode
     * @param session /
     * @return /
     */
    @PostMapping("gets")
    @Override
    public String gets(@RequestBody String request, HttpSession session) {
        return super.gets(request, session);
    }

    /**
     * 限制性HEAD，request和response都非明文，浏览器看不到，用于对安全性要求高的HEAD请求
     *
     * @param request 只用String，避免encode后未decode
     * @param session /
     * @return /
     */
    @PostMapping("heads")
    @Override
    public String heads(@RequestBody String request, HttpSession session) {
        return super.heads(request, session);
    }

    /**
     * 新增
     *
     * @param request 只用String，避免encode后未decode
     * @param session /
     * @return /
     */
    @PostMapping("post")
    @Override
    public String post(@RequestBody String request, HttpSession session) {
        return super.post(request, session);
    }

    /**
     * 修改
     *
     * @param request 只用String，避免encode后未decode
     * @param session /
     * @return /
     */
    @PostMapping("put")
    @Override
    public String put(@RequestBody String request, HttpSession session) {
        return super.put(request, session);
    }

    /**
     * 删除
     *
     * @param request 只用String，避免encode后未decode
     * @param session /
     * @return /
     */
    @PostMapping("delete")
    @Override
    public String delete(@RequestBody String request, HttpSession session) {
        return super.delete(request, session);
    }

    @PostMapping("reload")
    @Override
    public JSONObject reload(String type) {
        return super.reload(type);
    }
}
